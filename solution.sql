-- S03 ACTIVITY
USE blog_db;

-- #2
INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

SELECT * FROM users;

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00"),
VALUES (1, "Second Code", "Hello Earth", "2021-01-02 02:00:00"),
VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

SELECT * FROM posts;

-- #3
SELECT * FROM posts WHERE author_id = 1;

-- #4
SELECT email, datetime_created FROM users;

-- #5
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

SELECT content FROM posts WHERE id = 2;

-- #6
DELETE FROM users WHERE email = "johndoe@gmail.com";

SELECT * FROM users;


-- STRETCH GOALS
USE music_db;

-- #1
INSERT INTO users (username, password, full_name, contact_number, email, address)
VALUES ("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

SELECT * FROM users;

-- #2
INSERT INTO playlists (datetime_created, user_id) VALUES ("2023-09-20 08:00:00", 1);

SELECT * FROM playlists;

-- #3
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), VALUES (1, 2);

SELECT * FROM playlists_songs;